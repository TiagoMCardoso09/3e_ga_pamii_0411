import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { HomeSlidesCarouselComponent } from './components/home-slides-carousel/home-slides-carousel.component';
import { HomeSlidesComponent } from './components/home-slides/home-slides.component';
import { HomeModalComponent } from './components/home-modal/home-modal.component';
import { ModalPage } from './components/modal/modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [HomePage, HomeSlidesCarouselComponent, HomeSlidesComponent, HomeModalComponent, ModalPage],
  entryComponents: [ModalPage]
})
export class HomePageModule {}
