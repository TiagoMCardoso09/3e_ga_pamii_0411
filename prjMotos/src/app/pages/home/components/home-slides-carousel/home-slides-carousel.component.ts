import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-slides-carousel',
  templateUrl: './home-slides-carousel.component.html',
  styleUrls: ['./home-slides-carousel.component.scss'],
})
export class HomeSlidesCarouselComponent implements OnInit {

  optionsSlides = {
    loop: true,
    autoplay: true
  }

  slidesCarousel = [
    {
      img: 'https://content2.kawasaki.com/ContentStorage/KMB/HomePagePromo/18/d654def7-a062-46fe-bdf6-0835c1007a48.jpg?w=1920',
      titulo: 'Nossa rede de concessionárias está online para você.',
      subtitulo: 'Com o localizador fica fácil encontrar a Kawasaki mais perto de você.'
    },
    {
      img: 'https://www3.yamaha-motor.com.br/file/general/yma-home-racing-banner.png',
      titulo: 'YAMAHA RACING BRASIL',
      subtitulo: 'A competição faz parte do nosso DNA'
    }
  ]

  constructor() { }

  ngOnInit() {}

}
