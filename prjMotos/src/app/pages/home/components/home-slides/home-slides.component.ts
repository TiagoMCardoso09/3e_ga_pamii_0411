import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-slides',
  templateUrl: './home-slides.component.html',
  styleUrls: ['./home-slides.component.scss'],
})
export class HomeSlidesComponent implements OnInit {

  optionsSlides = {
    slidesPerView: 1.1,
    loop: false,
    autoplay: true
  }


  slides = [
    {
      img: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      entrega: 'Somente por encomenda',
      modelo: 'Yamaha MT03 2022',
      valor: '28.490',
      saibaMais: '#'
    },
    {
      img: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      entrega: 'Somente por encomenda',
      modelo: 'Yamaha MT03 2022',
      valor: '28.490',
      saibaMais: '#'
    },
    {
      img: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      entrega: 'Somente por encomenda',
      modelo: 'Yamaha MT03 2022',
      valor: '28.490',
      saibaMais: '#'
    },
    {
      img: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      entrega: 'Somente por encomenda',
      modelo: 'Yamaha MT03 2022',
      valor: '28.490',
      saibaMais: '#'
    },
    {
      img: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      entrega: 'Somente por encomenda',
      modelo: 'Yamaha MT03 2022',
      valor: '28.490',
      saibaMais: '#'
    }
  ]

  constructor() { }

  ngOnInit() {}

}
